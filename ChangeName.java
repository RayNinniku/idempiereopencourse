package tw.ninniku.training.process;

import java.util.logging.Level;
import org.compiere.model.MUser;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

public class ChangeName extends SvrProcess {

	//private static
	public int p_AD_User_ID = 0;
	public String p_Name = null;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("Name"))
				p_Name = para[i].getParameterAsString();
				
			else if (name.equals("AD_User_ID"))
				p_AD_User_ID = para[i].getParameterAsInt();

			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		//p_M_InOut_ID = getRecord_ID();

	}

	@Override
	protected String doIt() throws Exception {
		
		MUser user = new MUser(getCtx(), p_AD_User_ID, get_TrxName());
		
		user.setName(p_Name);
		user.save(get_TrxName());
		return "OK";
	}

}
