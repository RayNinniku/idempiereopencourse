-- FUNCTION: adempiere.hr_nightshift_rpt(numeric)

-- DROP FUNCTION adempiere.hr_nightshift_rpt(numeric);

CREATE OR REPLACE FUNCTION adempiere.hr_nightshift_rpt(
	pinstance numeric)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
/**
出勤異常查詢
*/
ResultStr VARCHAR (200);
roleaccesslevelwin VARCHAR (200);
sql VARCHAR (2000);
sqldel VARCHAR (2000);

p RECORD;
r RECORD;
r1 RECORD;
r2 RECORD;

--請假單
p_Absence_C_DocType_ID NUMERIC(10) := 1000106;
p_Overtime_C_DocType_ID NUMERIC(10) := 1000108;
--p_C_Invoice_ID NUMERIC(10) := 0;
p_DateAcct DATE;
p_DateAcct_TO DATE;
p_DateStep DATE;
p_DateFrom DATE;

p_Record_ID NUMERIC(10) := 0;
p_AD_User_ID NUMERIC(10) := 0;
p_User_ID NUMERIC(10) := 0;
p_AD_Client_ID NUMERIC(10) := 0;
p_AD_Org_ID NUMERIC(10) := 0;
p_AD_Org_ID_TO NUMERIC(10) := 0;
p_IsSOTrx VARCHAR (1) := 'Y';
p_C_DocType_ID NUMERIC(10) := 0;
p_C_DocType_ID_2 NUMERIC(10) := 0;

p_C_BPartner_Value VARCHAR (40) := 0;
p_C_DocType_Value VARCHAR (40) := 0;
p_C_BPartner_Value_TO VARCHAR (40) := '';
p_C_DocType_Value_TO VARCHAR (40) := '';

p_AccountValue VARCHAR (10);
p_AccountValue_TO VARCHAR (10);

p_MovementDate DATE;
p_MovementDate_TO DATE;
p_DateInvoiced DATE;
p_DateInvoiced_TO DATE;

v_message VARCHAR (400) := '';
v_documentno VARCHAR (30) := '';
v_fact_documentno VARCHAR (30) := '';
v_department VARCHAR (30) := '';
v_N_Department_ID NUMERIC(10) := 0;
v_tablename VARCHAR (50) := '';
v_count NUMERIC(10) := 0;
v_NextNo NUMERIC(10) := 0;
v_workinghoursfrom TIMESTAMP;
v_workinghoursto TIMESTAMP;
v_breaktimefrom TIMESTAMP;
v_breaktimeto TIMESTAMP;
v_mintime TIMESTAMP;
v_maxtime TIMESTAMP;
v_maxtime2 TIMESTAMP;
v_balance NUMERIC(10) := 0;
v_dateabsencefrom TIMESTAMP;
v_dateabsenceto TIMESTAMP;
v_isoff boolean := false;
v_workdays NUMERIC(10) := 0;
v_unpaiddays NUMERIC(10) := 0;
v_paiddays NUMERIC(10) := 0;
v_actualhours NUMERIC(10,1) := 0;
BEGIN

IF pinstance is null THEN 
	pinstance:=0;
END IF;

v_message :='程式開始:: 更新[呼叫程序]紀錄檔...開始執行時間(created)..程序執行中(isprocessing)..';
--IF pinstance > 0 THEN
BEGIN
ResultStr := 'PInstanceNotFound';
UPDATE adempiere.ad_pinstance
SET created = SYSDATE,
isprocessing = 'Y',
reslut = 0
WHERE ad_pinstance_id = pinstance_id;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;

END;

v_message :='OPEN ITEM Detail';

FOR p IN
(SELECT pi.record_id, pi.ad_client_id,pi.ad_org_id, pi.ad_user_id, 
pp.parametername,
pp.p_string, pp.p_number, pp.p_date,
pp.p_string_TO, pp.p_number_TO, pp.p_date_tO
FROM adempiere.ad_pinstance pi
LEFT OUTER JOIN adempiere.ad_pinstance_para pp ON(pi.ad_pinstance_id = pp.ad_pinstance_id)
WHERE pi.ad_pinstance_id = pinstance
ORDER BY pp.SeqNo)
LOOP
p_Record_ID := p.record_id;
p_User_ID := p.AD_User_id;
p_AD_Client_ID := p.AD_Client_ID;
p_AD_Org_ID := p.AD_Org_ID;

  IF p.parametername = 'DateAcct' THEN p_DateAcct = p.p_date;
     p_DateAcct_TO = p.p_date_to;
  ELSIF p.parametername = 'AD_Org_ID' THEN p_AD_Org_ID = p.p_number;
     p_AD_Org_ID_TO = p.p_number_to;
  ELSIF p.parametername = 'AD_User_ID' THEN p_AD_User_ID = p.p_number;
  END IF;

END LOOP;
--END IF;

--sqlins := 'INSERT INTO adempiere.t_an_shipment_h (ad_pinstance_id,t_an_shipment_h_id, bp_name) VALUES($1, $2, $3)';
--EXECUTE sqlins USING pinstance, 7, v_message;

IF p_User_ID IS NULL THEN 
   p_User_ID := 0; 
END IF;

IF pinstance = 1000000 THEN

p_DateAcct := '2018-01-01';
p_DateAcct_TO := '2018-01-19';
---p_AD_User_ID := 1000784;
END IF;

v_message :='Start Process';
--EXECUTE sqldel;
TRUNCATE t_hr_workdays;
TRUNCATE t_hr_nightshift_workday;
TRUNCATE t_hr_nightshift;

/* 保留字需要雙引號  */

/* 測試 CODE

select  hr_nightshift_rpt(1000000)
select * from t_hr_workdays
select * from t_hr_nightshift_workday 
select * from t_hr_nightshift
select  ad_pinstance_id, errormsg from  adempiere.ad_pinstance where ad_pinstance_id = 1000000
*/

/*
建立查詢範圍內的工作日.
select * from t_hr_workdays
create table t_hr_workdays as
select 0 ad_pinstance_id,  ad_client_id, ad_org_id , dateoff as datework from hr_holiday
*/
p_DateStep := p_DateAcct;
WHILE p_DateStep <= p_DateAcct_TO  LOOP
   --insert into t_hr_workdays (ad_pinstance_id,  ad_client_id, ad_org_id , datework)values(0,1000000,0,now());
   select count(*) into v_count from hr_holiday where dateoff = p_DateStep;
   if v_count = 0 then
	insert into t_hr_workdays (ad_pinstance_id,  ad_client_id, ad_org_id , datework)values(pinstance,p_AD_Client_ID,0,p_DateStep);
   end if;
    p_DateStep := p_DateStep + interval '1 day';	
END LOOP ;

--工作日總數
select count(*) into v_workdays from t_hr_workdays where ad_pinstance_id = pinstance;

/* 列出所有 有卡號的 User */
FOR r IN (
 --select * from t_hr_attendance_userdetail
 --drop table t_hr_attendance_userdetail
 --create table t_hr_attendance_userdetail as 
 select 
 p_AD_Client_ID ad_client_id , p_AD_Org_ID ad_org_id, '上班'::VARCHAR (10) IO, 
 now() today,now() firsttime, now() lasttime , 
 now() absencefrom , now() absenceto,hs.hr_shift_id,u.c_campaign_id,
 0.1 balance,
 u.cardnumber, u.name,u.value,u.ad_user_id,hs.workinghoursfrom, hs.workinghoursto, hs.breaktimefrom, hs.breaktimeto 
 from ad_user u 
 inner join hr_nightshift_list nl on u.ad_user_id = nl.ad_user_id
 left join hr_shift hs on u.hr_shift_id = hs.hr_shift_id
 where  true
 and u.isactive = 'Y' 
 and  (u.jobstatus = '在職') 
 and (p_AD_User_ID is null OR p_AD_User_ID = 0 OR u.AD_User_ID = p_AD_User_ID)
 order by u.value
)LOOP

--- 列出所有工作日
	FOR r1 IN (
		select datework from t_hr_workdays order by datework
	)LOOP

	
        v_workinghoursfrom := r1.datework;
        v_workinghoursfrom := v_workinghoursfrom  + interval '1 hour' * date_part('hour', r.workinghoursfrom);
        v_workinghoursfrom := v_workinghoursfrom  + interval '1 minute' * date_part('minute', r.workinghoursfrom);
        v_workinghoursfrom := v_workinghoursfrom  + interval '1 second' * 59;

        v_workinghoursto := r1.datework;
        v_workinghoursto := v_workinghoursto  + interval '1 hour' * date_part('hour', r.workinghoursto);
        v_workinghoursto := v_workinghoursto  + interval '1 minute' * date_part('minute', r.workinghoursto);

	v_breaktimefrom := r1.datework;
        v_breaktimefrom := v_breaktimefrom  + interval '1 hour' * date_part('hour', r.breaktimefrom);
        v_breaktimefrom := v_breaktimefrom  + interval '1 minute' * date_part('minute', r.breaktimefrom);

	v_breaktimeto := r1.datework;
        v_breaktimeto := v_breaktimeto  + interval '1 hour' * date_part('hour', r.breaktimeto);
        v_breaktimeto := v_breaktimeto  + interval '1 minute' * date_part('minute', r.breaktimeto);

	/** 檢查輪班表有沒有資料 **/

		FOR r2 IN (

			SELECT * FROM HR_RotatingShift Where AD_User_ID = r.ad_user_id and r1.datework between validfrom and validto
		)LOOP
			v_workinghoursfrom := r1.datework;
			v_workinghoursfrom := v_workinghoursfrom  + interval '1 hour' * date_part('hour', r2.workinghoursfrom);
			v_workinghoursfrom := v_workinghoursfrom  + interval '1 minute' * date_part('minute', r2.workinghoursfrom);
			v_workinghoursfrom := v_workinghoursfrom  + interval '1 second' * 59;

			v_workinghoursto := r1.datework;
			v_workinghoursto := v_workinghoursto  + interval '1 hour' * date_part('hour', r2.workinghoursto);
			v_workinghoursto := v_workinghoursto  + interval '1 minute' * date_part('minute', r2.workinghoursto);

			v_breaktimefrom := r1.datework;
			v_breaktimefrom := v_breaktimefrom  + interval '1 hour' * date_part('hour', r2.breaktimefrom);
			v_breaktimefrom := v_breaktimefrom  + interval '1 minute' * date_part('minute', r2.breaktimefrom);

			v_breaktimeto := r1.datework;
			v_breaktimeto := v_breaktimeto  + interval '1 hour' * date_part('hour', r2.breaktimeto);
			v_breaktimeto := v_breaktimeto  + interval '1 minute' * date_part('minute', r2.breaktimeto);

			EXIT;
		END LOOP;

	/**
	跨夜判斷
	**/
	if v_workinghoursto < v_workinghoursfrom then
		v_workinghoursto := v_workinghoursto + interval '1 day';
	end if;

	if v_breaktimefrom < v_workinghoursfrom then
		v_breaktimefrom := v_breaktimefrom + interval '1 day';
		v_breaktimeto := v_breaktimeto + interval '1 day';
	end if;
	v_message :='breaktine setup done';

	--select * from hr_recordtable
        -- 上班
        v_dateabsencefrom := null;
	v_dateabsenceto := null;
	v_actualhours := 0;
	v_isoff = false;
	--打卡時間--
	select min(actiontime) into v_mintime 
	from hr_recordtable where 
	cardnumber  in (select cardnumber from hr_cardnumber where ad_user_id = r.ad_user_id and r1.datework >= validfrom and (r1.datework <= validto or validto is null )) 
	and date_trunc('day',actiontime) = r1.datework and actiontime > v_workinghoursfrom - interval '1 hour' * 3;

        select max(actiontime) into v_maxtime 
	from hr_recordtable where 
	cardnumber  in (select cardnumber from hr_cardnumber where ad_user_id = r.ad_user_id and r1.datework >= validfrom and (r1.datework <= validto or validto is null )) 
	and date_trunc('day',actiontime) >= r1.datework
	and actiontime < v_workinghoursto  +  interval '1 hour' * 6;

	v_message :='打卡時間';

        -- 如果有請假，要重設當天上下班時間.
        -- hs.breaktimefrom, hs.breaktimeto
	if v_mintime is null OR v_mintime > v_workinghoursfrom OR v_maxtime is null OR v_maxtime < v_workinghoursto then
		v_message :='打卡異常';
		FOR r2 IN (
			select *  from hr_transaction 
			where ad_user_id = r.ad_user_id 
			and leavehours < 0
			and isactive = 'Y'
			and c_doctype_id = p_Absence_C_DocType_ID
			and dateabsencefrom is not null
			and dateabsenceto is not null
			and  date_trunc('day',r1.datework) between date_trunc('day',dateabsencefrom) and date_trunc('day',dateabsenceto)
			
		)LOOP
			v_message :='有請假';
			v_dateabsencefrom := r2.dateabsencefrom;
			v_dateabsenceto := r2.dateabsenceto;
		        -- 如果有請假時間(起)早於標準上班時間, 請假時間(晚於)下班時間,整天休假
			if r2.dateabsencefrom <= v_workinghoursfrom AND  r2.dateabsenceto >= v_workinghoursto then
				v_isoff = true;
			end if;
			-- 如果有請假時間(起)早於標準上班時間, 請假時間(迄)早於下班時間,請上午. 上班時間改為請假時間(迄)
			if r2.dateabsencefrom <= v_workinghoursfrom AND  r2.dateabsenceto < v_workinghoursto then
				v_workinghoursfrom := r2.dateabsenceto ;
				--v_workinghoursfrom := v_workinghoursfrom + interval '1 second' * 59;

			end if;

			-- 如果有請假時間(起)晚於標準上班時間, 請假時間(迄)晚於下班時間,請下午. 下班時間改為請假時間(起)
			if r2.dateabsencefrom > v_workinghoursfrom AND  r2.dateabsenceto >= v_workinghoursto then
				v_workinghoursto := r2.dateabsencefrom ;
			end if;

			-- 休息時間調整
			-- 調整時間>= 休息時間,上班時聞改為休息時間(迄)
			if v_workinghoursfrom >= v_breaktimefrom and    v_workinghoursfrom <  v_breaktimeto and  v_workinghoursto >= v_breaktimeto then
				v_workinghoursfrom := v_breaktimeto;
				--v_workinghoursfrom := v_workinghoursfrom + interval '1 second' * 59;
			end if;
			-- 調整時間<= 休息時間,下班時聞改為休息時間(起)
			if v_workinghoursfrom <= v_breaktimefrom and v_workinghoursto > v_breaktimefrom and v_workinghoursto <= v_breaktimeto then
				v_workinghoursto := v_breaktimefrom;
			end if;
			if  date_part('second', v_workinghoursfrom) = 0 then 
				v_workinghoursfrom := v_workinghoursfrom + interval '1 second' * 59;
			end if;
			
			v_message :='調整完上班時間';
			EXIT;
		END LOOP;
		
	end if;

        if not v_isoff AND ( v_mintime is null OR v_mintime > v_workinghoursfrom) then
	   v_message :='非請整天 上班異常';
	   -- 檢查是否有請假
	   -- select * from hr_transaction 
		v_balance := 1;

	   -- 檢查是否有登記未打卡
		if v_balance = 1 then
			v_message :='檢查是否有登記未打卡';
			FOR r2 IN (
				select *  from hr_forget 
				where ad_user_id = r.ad_user_id 
				and datefrom is not null
				and date_trunc('day',dateacct)  = date_trunc('day',r1.datework)
				and datefrom <= v_workinghoursfrom
				--and date_trunc('day',dateacct)  = date_trunc('day',v_workinghoursfrom) 
				--and (date_part('hour', datefrom) * 100 + date_part('minute', datefrom)) <= (date_part('hour', v_workinghoursfrom) * 100 + date_part('minute', v_workinghoursfrom)) 
			)LOOP
				v_message :='hr_forget datefrom';
				v_dateabsencefrom = date_trunc('day',r2.dateacct)  + interval '1 hour' * date_part('hour', r2.datefrom) + interval '1 minute' * date_part('minute', r2.datefrom);
				v_dateabsencefrom = r2.datefrom;
				v_mintime = r2.datefrom;
				-- r2.datefrom;
				v_dateabsenceto = date_trunc('day',r2.dateacct)  + interval '1 hour' * date_part('hour', r2.dateto) + interval '1 minute' * date_part('minute', r2.dateto);
				v_balance := 0;
			END LOOP; 
		end if;
	   	
	else 
	    v_balance := 0;
	end if;    

        -- 下班

        if not v_isoff and (v_maxtime is null OR v_maxtime < v_workinghoursto) then
            -- 檢查是否有請假
		v_balance := 1;
		-- 檢查是否有登記未打卡
		if v_balance = 1 then
			v_message :='檢查是否有登記未打卡';
			FOR r2 IN (
				select *  from hr_forget 
				where ad_user_id = r.ad_user_id 
				and dateto is not null
				and date_trunc('day',dateacct)  = date_trunc('day',r1.datework)
				and dateto >= v_workinghoursto
				--and date_trunc('day',dateacct)  = date_trunc('day',v_workinghoursto) 
				--and (date_part('hour', dateto) * 100 + date_part('minute', dateto)) >= (date_part('hour', v_workinghoursto) * 100 + date_part('minute', v_workinghoursto)) 
			)LOOP
				v_message :='hr_forget datefrom';
				v_maxtime = r2.dateto;
				v_dateabsenceto = r2.dateto;
				v_balance := 0;
			END LOOP; 
		end if;

	else 
	    v_balance := 0;
	end if;    

	-- 校正上下班時間
	if v_mintime <= v_workinghoursfrom then
		v_mintime = v_workinghoursfrom - interval '1 second' * 59;
	end if;

	if v_maxtime >= v_workinghoursto then
		v_maxtime = v_workinghoursto;
	end if;
	-- 檢查是否加班
	v_message :='檢查是否有加班';
	FOR r2 IN (
	select *  from hr_transaction 
			where ad_user_id = r.ad_user_id 
			and c_doctype_id = p_Overtime_C_DocType_ID
			and isactive = 'Y'
			and dateabsencefrom is not null
			and dateabsenceto is not null
			and  v_workinghoursto between dateabsencefrom and dateabsenceto
	)LOOP
			-- 重抓下班時間
			select max(actiontime) into v_maxtime2 
				from hr_recordtable where cardnumber = r.cardnumber 
				and date_trunc('day',actiontime) >= r1.datework
				and actiontime > r2.dateabsencefrom
				and actiontime < r2.dateabsenceto +  interval '1 hour' * 3;

				if r2.dateabsencefrom >= v_workinghoursto and r2.dateabsenceto > v_workinghoursto and v_maxtime2 > r2.dateabsenceto then
					v_balance := 0;
					v_dateabsencefrom := r2.dateabsencefrom;
					v_dateabsenceto := r2.dateabsenceto;
					v_maxtime := v_maxtime2;
				end if;
			if v_maxtime > r2.dateabsenceto then
				v_maxtime = r2.dateabsenceto;
			end if; 
	END LOOP; 
	-- 計算真時時數
	if v_maxtime is not null and v_mintime is not null then 

		v_actualhours =  (EXTRACT(EPOCH FROM v_maxtime) - EXTRACT(EPOCH FROM v_mintime)) ;
		if v_maxtime >= v_breaktimeto then

			v_actualhours = v_actualhours - (EXTRACT(EPOCH FROM v_breaktimeto) - EXTRACT(EPOCH FROM v_breaktimefrom));

		end if;
		v_actualhours = v_actualhours / 3600;

	end if;
		INSERT INTO t_hr_nightshift_workday(
		ad_pinstance_id, ad_client_id, ad_org_id, io, today, firsttime, lasttime, absencefrom, 
		absenceto, balance, cardnumber, name, value, ad_user_id, workinghoursfrom, 
		workinghoursto, breaktimefrom, breaktimeto,actualhours)
		VALUES (pinstance,r.ad_client_id, r.ad_org_id, null, r1.datework, v_mintime, v_maxtime, v_dateabsencefrom, 
		v_dateabsenceto, v_balance, r.cardnumber, r.name, r.value, r.ad_user_id, v_workinghoursfrom, 
		v_workinghoursto, v_breaktimefrom, v_breaktimeto,v_actualhours);
        -- workday end
	END LOOP;
	
	select count(*) into v_unpaiddays 
	from t_hr_nightshift_workday x where actualhours < 6 and ad_user_id = r.ad_user_id;

	select count(*) into v_paiddays 
	from t_hr_nightshift_workday x where actualhours >= 6 and ad_user_id = r.ad_user_id;

	INSERT INTO t_hr_nightshift(
            ad_pinstance_id, ad_client_id, ad_org_id, ad_user_id, value, 
            c_campaign_id, hr_shift_id, name, workdays, unpaiddays, paiddays, 
            total)
	VALUES (pinstance,r.ad_client_id, r.ad_org_id, r.ad_user_id, r.value, 
            r.c_campaign_id, r.hr_shift_id, r.name, v_workdays, v_unpaiddays, v_paiddays, 
            (v_unpaiddays + v_paiddays));

	
END LOOP;

-- select * from t_hr_nightshift_workday
/*
drop table t_hr_nightshift
create table t_hr_nightshift as 
select 0 ::numeric(10,0) ad_pinstance_id, 0 ::numeric(10,0) ad_client_id,0 ::numeric(10,0) ad_org_id
,ad_user_id,value, 
(select c_campaign_id from ad_user where ad_user_id = x.ad_user_id) c_campaign_id, 0 :: numeric(10,0) hr_shift_id
,name, 0 ::numeric(10,0) workdays, 0 :: numeric(10,0) unpaiddays, 0 :: numeric(10,0) paiddays,0 :: numeric(10,0) total
from t_hr_nightshift_workday x where actualhours < 6 group by ad_user_id,value,name
*/
-- drop table t_hr_attendance_userdetail
--create table t_hr_nightshift_workday as select * , 0::numeric(10,1) actualhours from t_hr_attendance_userdetail

v_message :='END Process';

IF pinstance > 0 THEN
BEGIN
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 1,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
END IF;

EXCEPTION

WHEN OTHERS THEN
--sqlins := 'INSERT INTO adempiere.t_an_shipment (ad_pinstance_id, t_an_shipment_id, bp_name) VALUES($1, $2, $3)';
--EXECUTE sqlins USING pinstance, 9, v_message;
--v_message :='例外錯誤。。。';
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 0,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;

END;
$BODY$;

ALTER FUNCTION adempiere.hr_nightshift_rpt(numeric)
    OWNER TO dev;
