
CREATE OR REPLACE VIEW rv_topsales_v AS 
 SELECT o.ad_client_id,
    o.ad_org_id,
    o.salesrep_id,
    sum(o.grandtotal) AS sum,
    rank() OVER (ORDER BY (sum(o.grandtotal)) DESC) AS rank_number,
    u.name,
    u.title
   FROM c_order o
     JOIN ad_user u ON o.salesrep_id = u.ad_user_id
  GROUP BY o.ad_client_id, o.ad_org_id, o.salesrep_id, u.name, u.title;

ALTER TABLE rv_topsales_v
  OWNER TO adempiere;